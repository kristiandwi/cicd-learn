ARG APP_BASE_DIR="."
ARG PHP_VERSION="8.1"
ARG PHP_ALPINE_VERSION="3.18"
ARG NGINX_VERSION="1.21"

# Inisialisasi Composer untuk multi stage build
FROM composer:2 as composer 

# Section untuk build base image PHP-FPM
FROM php:$PHP_VERSION-fpm-alpine$PHP_ALPINE_VERSION as base 

# default direktori untuk source code
WORKDIR /app

# update repo , dan untuk menambah docker-ext yang diperlukan
RUN apk add --no-cache --virtual .build-deps zip
#RUN docker-php-ext-install opcache 

# hapus user bawaan dari alpine dan membuat user baru
RUN deluser --remove-home www-data && adduser -u1000 -D www-data 
RUN mkdir -p /var/www/.composer /app && chown -R www-data:www-data /app /var/www/.composer 
RUN mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini

COPY docker/php/base-*   $PHP_INI_DIR/conf.d

# copy composer dari image composer yang sudah dideklarasikan diatas
COPY --from=composer /usr/bin/composer /usr/bin/composer

# PHP environtment
ENV APP_ENV prod
ENV APP_DEBUG 0

# berpindah memakai user www-data
USER www-data

# Validate FPM config 
RUN php-fpm -t

CMD ["php-fpm"]


# ======================================================================================================================
#                                                  --- Vendor ---
# ======================================================================================================================

FROM composer as vendor

ARG PHP_VERSION
ARG APP_BASE_DIR
WORKDIR /app

# Copy dependency
COPY $APP_BASE_DIR/composer.json composer.json
COPY $APP_BASE_DIR/composer.lock composer.lock

# Set versi PHP untuk running composer
RUN composer config platform.php ${PHP_VERSION}; \
    # Install Dependencies
    composer install -n --no-progress --ignore-platform-reqs --no-dev --prefer-dist --no-scripts --no-autoloader

# ======================================================================================================================
#                                                  --- Akhir Vendor ---
# ======================================================================================================================


# ======================================================================================================================
#                                                  --- PRODUCTION IMAGE---
# ======================================================================================================================

# Menggunakan base image untuk multi stage build

FROM  base as production_image
ARG APP_BASE_DIR

# Copy file config php untuk image production
USER root
COPY docker/php/prod-*   $PHP_INI_DIR/conf.d/

# Ganti user menjadi www-data
USER www-data

# ----------------------------------------------- Production Config -----------------------------------------------------

# Copy Vendor yang telah digenerate oleh composer image
COPY --chown=www-data:www-data --from=vendor /app/vendor /app/vendor

# Copy source code
COPY --chown=www-data:www-data $APP_BASE_DIR/ .

# Menjalankan Composer install
RUN cp .env.example .env
RUN composer install --optimize-autoloader --apcu-autoloader --no-dev -n --no-progress
RUN php artisan key:generate
CMD ["php-fpm"]

# ======================================================================================================================
#                                            --- AKHIR PRODUCTION IMAGE---
# ======================================================================================================================
